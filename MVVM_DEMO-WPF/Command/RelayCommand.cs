﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace MVVM_DEMO_WPF.Command
{
    public class RelayCommand : ICommand
    {
        Action<object> executeAction;
        Func<object, bool> canExecute; // variables


        public RelayCommand(Action<object> executeAction, Func<object, bool> canExecute)
        {
            this.canExecute = canExecute;
            this.executeAction = executeAction; // initialized variables here
        }

        public event EventHandler CanExecuteChanged
        {
            add
            {
               // if (!canExecuteCache)
                //{
                    CommandManager.RequerySuggested += value;
                //}
            }

            remove
            {
                CommandManager.RequerySuggested -= value;
            }
        }



        public void Execute(object parameter)
        {
            executeAction(parameter);
        }

        public bool CanExecute(object parameter)
        {
            if(canExecute == null) {
                return true;
            }
            else
            {
                return canExecute(parameter);
            }
        }
    }
}
 

    


