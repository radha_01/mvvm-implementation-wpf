﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MVVM_DEMO_WPF.Model
{
    public class Person : INotifyPropertyChanged , IDataErrorInfo
    {
		private string fName;

		public string FName
		{
			get { return fName; }
			set { fName = value; 
                OnPropertyChanged("FName");
            }
		}

        private string lName;

        public string LName
        {
            get { return lName; }
            set { lName = value;
                OnPropertyChanged("LName");
            }
        }

        private string fullName;
        public string FullName
        {
            get { return fullName = FName + " " + LName;
            }
            set {
                if (fullName != value)
                { fullName = value; }
                }
        } // dataErrorInfo
        public DateTime DateAdded { get; set; }

        public string Error => throw new NotImplementedException();

        public string this[string PropertyName]
        {
            get
            { string result = String.Empty;
                switch (PropertyName)
                {
                    case "FName":
                        if(string.IsNullOrEmpty(FName))
                        
                            result = "First Name is required!";

                        break;

                    case "LName":
                        if (string.IsNullOrEmpty(LName))

                            result = "Last Name is required!";

                        break;


                }
                return result;
            }
        }
        
        
        //

        public event PropertyChangedEventHandler PropertyChanged;
         
        private void OnPropertyChanged(string property) // to raise the below event by making method 
        {
            PropertyChangedEventHandler ph = PropertyChanged;
            if (ph != null)
            {
                ph(this, new PropertyChangedEventArgs(property));
            }
        }


    }
}
