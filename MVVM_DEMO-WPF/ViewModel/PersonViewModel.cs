﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using MVVM_DEMO_WPF.Command;
using MVVM_DEMO_WPF.Model; //becos my person class present into the model namespace

namespace MVVM_DEMO_WPF.ViewModel
{ 
    public class PersonViewModel : INotifyPropertyChanged // used it here because the collection class in vm tht will notify to ui .. soul prpose of inotify property chain interface is to notify ui or provide the chnge.... propogating those chnges from vm to ui
    { 
        public PersonViewModel() 
        {
            Person = new Person();
            Persons = new ObservableCollection<Person>();
        }
        private Person _person;
        public Person Person {  //property
          
            get { return _person; }
            set { _person = value; 
                //NotifyPropertyChanged("Person");

            } // data from model also needs to b passed through viewmodel nd reach to the ui exposing a property from model to viewmodel tht this property in turn communicate to view 
        }
        private ObservableCollection<Person> _persons; // the moment any person added or removed on to the list this will b notified to ui
        private ICommand _SubmitCommand;

        public ObservableCollection<Person> Persons // this is used explicitly
        {
            get 
            { 
                return _persons; 
            }
            set
            {
                _persons = value;
                //NotifyPropertyChanged("Persons");
            }
        }


        public ICommand SubmitCommand
        {
            get
            {
                if (_SubmitCommand == null)
                {
                    _SubmitCommand = new RelayCommand(SubmitExecute, canSubmitExecute);
                }
                return _SubmitCommand;
            }
        }
        private void SubmitExecute(object parameter)
        {
            Persons.Add(Person);
        }

        private bool canSubmitExecute(object parameter)
        {
            if (string.IsNullOrEmpty(Person.FName) || string.IsNullOrEmpty(Person.LName))
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        protected void NotifyPropertyChanged(string propertyName) // to raise the below event by making method 
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

    }

}
